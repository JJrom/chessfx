package sample;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author Joonas
 */
public class Game {
    static Piece Board[][] = new Piece[8][8];
    HashSet<Piece> whitePieceSet = new HashSet();
    HashSet<Piece> blackPieceSet = new HashSet();
    King blackKing;
    King whiteKing;
    GridPane boardPane;

    /**
     *
     * @param isWhite side of the checking party, 1= White 0 =Black
     * @return HashSet that contains all enemy pieces
     */
    public HashSet<Piece> returnEnemySet(int isWhite) {
        if (isWhite == 1) {
            return blackPieceSet;
        } else {
            return whitePieceSet;
        }
    }

    /**
     * Prints the board for debugging
     */
    public void printBoard() {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (Board[x][y] != null) {
                    Piece p = Board[x][y];
                    System.out.print(p.returnName());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println("");
        }
        System.out.println("");
    }

    /**
     *
     * @return 8x8 array of pieces used for the current game
     */
    public Piece[][] returnBoard() {
        return this.Board;
    }

    /**
     *
     * @param positionOnXAxis Position on x axis to check
     * @param positionOnYAxis Position on Y axis to check
     * @return True if board contains piece in position, else false
     */

    public boolean hasPiece(int positionOnXAxis, int positionOnYAxis) {
        return Board[positionOnXAxis][positionOnYAxis] != null;
    }

    /**
     * Attempts to move the piece. Used to prevent illegal moves that leave king in check and for moving.
     *
     * @param startingPositionY Position of Piece to move on y axis
     * @param startingPositionX Position of Piece to move on x axis
     * @param targetPositionY  Position to move into y axis
     * @param targetPositionX  Position to move into X axis
     * @param gamePane Pane for the gameboard
     * @return True if move was success, else false
     */
    public boolean movePiece(int startingPositionY, int startingPositionX, int targetPositionY, int targetPositionX, GridPane gamePane) {

        Piece tmp = Board[startingPositionY][startingPositionX];
        Piece tmpEnd = Board[targetPositionY][targetPositionX];
        countValues();
        if (Board[startingPositionY][startingPositionX] != null) {

            if (Board[startingPositionY][startingPositionX] instanceof King) {
                King k = (King) Board[startingPositionY][startingPositionX];
                //System.out.println("TX TY---------" + targetPositionY + " " + targetPositionX);
                if (((targetPositionY == 7 && targetPositionX == 6) || (targetPositionY == 7 && targetPositionX == 2)) || ((targetPositionY == 0 && targetPositionX == 6) || (targetPositionY == 0 && targetPositionX == 2))) {
                    if ((targetPositionY == 7 && targetPositionX == 6) && !k.hasMoved) {
                        System.out.println(Board[5][7] == null);
                        if (opponentCanMoveToTarget(startingPositionY, startingPositionX, 7, 5, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board) || opponentCanMoveToTarget(startingPositionY, startingPositionX, 7, 4, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board)) {
                            // System.out.println("Cant castle through check");
                            return false;
                        }

                    } else if ((targetPositionY == 7 && targetPositionX == 2) && !k.hasMoved) {
                        if (opponentCanMoveToTarget(startingPositionY, startingPositionX, 7, 3, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board) || opponentCanMoveToTarget(startingPositionY, startingPositionX, 7, 4, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board)) {
                           //  System.out.println("Cant castle through check");
                            return false;
                        }
                    } else if ((targetPositionY == 0 && targetPositionX == 6) && !k.hasMoved) {
                        if (opponentCanMoveToTarget(startingPositionY, startingPositionX, 0, 5, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board) || opponentCanMoveToTarget(startingPositionY, startingPositionX, 0, 4, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board)) {
                            //  System.out.println("Cant castle through check");
                            return false;
                        }
                    } else if ((targetPositionY == 2 && targetPositionX == 0) && !k.hasMoved) {
                        if (opponentCanMoveToTarget(startingPositionY, startingPositionX, 0, 3, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board) || opponentCanMoveToTarget(startingPositionY, startingPositionX, 0, 4, returnEnemySet(Board[startingPositionY][startingPositionX].getIsWhite()), Board)) {
                            // System.out.println("Cant castle through check");
                            return false;
                        }

                    }

                    if (opponentCanMoveToTarget(startingPositionY, startingPositionX, targetPositionY, targetPositionX, returnEnemySet(tmp.getIsWhite()), Board)) {

                        if (!stillInCheck(startingPositionY, startingPositionX, targetPositionY, targetPositionX, tmp.getIsWhite(), (King) tmp)) {
                            if (tmp.canMove(targetPositionY, targetPositionX, Board)) {
                                tmp.move(startingPositionY,startingPositionX,targetPositionY, targetPositionX, Board);
                                countValues();
                                setAllPics(gamePane);
                                return true;
                            }

                        }
                        return false;
                    } else {

                        if (tmp.canMove(targetPositionY, targetPositionX, Board)) {
                            tmp.move(startingPositionY,startingPositionX,targetPositionY, targetPositionX, Board);
                            countValues();
                            setAllPics(gamePane);
                            return true;
                        }
                        return false;
                    }

                }
                if (opponentCanMoveToTarget(startingPositionY, startingPositionX, k.currentPositionY, k.currentPositionX, returnEnemySet(tmp.getIsWhite()), Board)) {

                    if (stillInCheck(startingPositionY, startingPositionX, targetPositionY, targetPositionX, k.getIsWhite(), k)) {

                        Board[targetPositionY][targetPositionX] = tmpEnd;
                        Board[startingPositionY][startingPositionX] = tmp;
                        countValues();
                        setAllPics(gamePane);
                        return false;

                    } else {
                        Board[targetPositionY][targetPositionX] = null;
                        countValues();
                        setAllPics(gamePane);
                        tmp.move(startingPositionY,startingPositionX,targetPositionY, targetPositionX, Board);

                        countValues();
                        setAllPics(gamePane);
                        return true;
                    }
                }
                boolean moved = tmp.hasMoved;
                Piece tmp2 = null;
                if (tmp.canMove(targetPositionY, targetPositionX, Board)) {
                    if (Board[targetPositionY][targetPositionX] != null) {
                        tmp2 = Board[targetPositionY][targetPositionX];
                    }
                    tmp.move(startingPositionY,startingPositionX,targetPositionY, targetPositionX, Board);

                } else {
                    countValues();
                    setAllPics(gamePane);
                    return false;
                }
                if (!opponentCanMoveToTarget(startingPositionY, startingPositionX, targetPositionY, targetPositionX, returnEnemySet(tmp.getIsWhite()), Board)) {
                    Board[targetPositionY][targetPositionX] = null;
                    Board[targetPositionY][targetPositionX] = tmp;
                    countValues();
                    setAllPics(gamePane);
                    return true;
                }
                if (tmp2 != null) {
                    Board[targetPositionY][targetPositionX] = tmp2;
                    tmp2.currentPositionY = targetPositionY;
                    tmp2.currentPositionX = targetPositionX;
                }
                tmp.move(startingPositionY,startingPositionX,startingPositionY, startingPositionX, Board);
                tmp.hasMoved = moved;
                countValues();
                setAllPics(gamePane);
                return false;
            }

            King ku;
            if (tmp.getIsWhite() == 1) {
                ku = whiteKing;
            } else {
                ku = blackKing;
            }

            if (opponentCanMoveToTarget(startingPositionY, startingPositionX, ku.currentPositionY, ku.currentPositionX, returnEnemySet(tmp.getIsWhite()), Board)) {

                if (stillInCheck(startingPositionY, startingPositionX, targetPositionY, targetPositionX, ku.getIsWhite(), ku)) {
                     System.out.println("Move out of check");
                    Board[targetPositionY][targetPositionX] = tmpEnd;
                    Board[startingPositionY][startingPositionX] = tmp;
                    countValues();
                    setAllPics(gamePane);
                    return false;

                } else {

                    tmp.move(startingPositionY,startingPositionX,targetPositionY,targetPositionX, Board);
                    if (tmp instanceof Pawn && tmp.getIsWhite() == 0 && tmp.currentPositionY == 0) {
                        tmp = new Queen(Board, targetPositionY, targetPositionX, 0);
                    } else if (tmp instanceof Pawn && tmp.getIsWhite() == 1 && tmp.currentPositionY == 7) {
                        tmp = new Queen(Board, targetPositionY, targetPositionX, 1);
                    }
                    System.out.println("Now out of check");
                    countValues();
                    setAllPics(gamePane);

                    return true;
                }
            }
            countValues();

            if (Board[startingPositionY][startingPositionX].canMove(targetPositionY, targetPositionX, Board)) {

                countValues();
                if (tmp.getIsWhite() == 1) {
                    ku = whiteKing;
                } else {
                    ku = blackKing;
                }

                // if Opponent can move to the current position of king return false
                if (opponentCanMoveToTarget(startingPositionY, startingPositionX, ku.currentPositionY, ku.currentPositionX, returnEnemySet(tmp.getIsWhite()), Board)) {
                    //System.out.println("Enemy can move to king position");
                    Board[startingPositionY][startingPositionX] = tmp;
                    Board[targetPositionY][targetPositionX] = tmpEnd;
                    countValues();
                    setAllPics(gamePane);
                    return false;

                }
                //Todo: migrate promotion logic to Pawn.move, add underpromotion
                if (tmp instanceof Pawn && tmp.getIsWhite() == 0 && targetPositionY == 7) {
                    tmp = new Queen(Board, targetPositionY, targetPositionX, 0);
                   // System.out.println("Promotes to queen");
                } else if (tmp instanceof Pawn && tmp.getIsWhite() == 1 && targetPositionY == 0) {
                    tmp = new Queen(Board, targetPositionY, targetPositionX, 1);
                   // System.out.println("Promotes to queen");
                }
                tmp.move(startingPositionY,startingPositionX,targetPositionY, targetPositionX, Board);
                countValues();
                setAllPics(gamePane);
                return true;
            }
        }
        //return false by default
        countValues();
        setAllPics(gamePane);
        return false;
    }

    /**
     * Checks if opponent can move to spesified position
     * @param checkThisY Position to check on x axis
     * @param checkThisX Position to check on Y axis
     * @param targetPositionY
     * @param targetPositionX
     * @param enemyHashSet hashSet containing enemy pieces
     * @param board Piece[][] array representing chess board
     * @return True if opponent piece can move to target position
     */
    boolean opponentCanMoveToTarget(int checkThisY, int checkThisX, int targetPositionY, int targetPositionX, HashSet<Piece> enemyHashSet, Piece[][] board) {
        Piece tmp = null;
        King k = null;
        int isWhite = -1;
        for (Piece piece : enemyHashSet) {
            isWhite = piece.getIsWhite();
        }

        if (isWhite == 0) {
            k = whiteKing;
        } else {
            k = blackKing;
        }

        if (board[k.currentPositionY][k.currentPositionX] != null) {
            tmp = board[checkThisY][checkThisX];
            if (tmp instanceof King) {
                // This prevents king from blocking a check to itself.
                board[checkThisY][checkThisX] = null;
            }
        }

        for (Piece p : enemyHashSet) {
            if ((p instanceof Pawn) && tmp instanceof King) {
                //if enemy piece is a pawn and tmp is king, king needs to be on the board for pawn capture logic to work
                board[checkThisY][checkThisX] = tmp;
            }
            if (p.canMove(targetPositionY, targetPositionX, board) || p.canMove(k.currentPositionY, k.currentPositionX, board)) {
                board[checkThisY][checkThisX] = tmp;
                //System.out.println("Piece " + p.returnName() + " xy " + p.currentPositionX + " " + p.currentPositionY + "can move here");
                return true;
            }
            board[checkThisY][checkThisX] = null;
        }
        if (tmp != null && board[checkThisY][checkThisX] == null) {
            board[checkThisY][checkThisX] = tmp;
        }

        return false;
    }

    /**
     * Returns true if in check after move has been made, else false
     * @param currentPositionY
     * @param currentPositionX
     * @param targetPositionY
     * @param targetPositionX
     * @param isWhite 1 == white, 0 == black
     * @param k
     * @return True if in check after move, else false
     */
    boolean stillInCheck(int currentPositionY, int currentPositionX, int targetPositionY, int targetPositionX, int isWhite, King k) {
        // System.out.println("Checking if still in check");
        boolean currentPieceHasMoved = Board[currentPositionY][currentPositionX].hasMoved;
        Piece tmpPieceFromTargetPosition = Board[targetPositionY][targetPositionX];
        Piece tmpPieceFromCurrentPosition = Board[currentPositionY][currentPositionX];
        Board[currentPositionY][currentPositionX].move(currentPositionY,currentPositionX,targetPositionY, targetPositionX, Board);
        tmpPieceFromCurrentPosition.hasMoved = currentPieceHasMoved;
        countValues();

        printBoard();
        if (opponentCanMoveToTarget(currentPositionY, currentPositionX, k.currentPositionY, k.currentPositionX, returnEnemySet(k.getIsWhite()), Board)) {
            //System.out.println("Still in check");
            printBoard();
            Board[currentPositionY][currentPositionX] = tmpPieceFromCurrentPosition;
            Board[targetPositionY][targetPositionX] = tmpPieceFromTargetPosition;
            tmpPieceFromCurrentPosition.currentPositionY = currentPositionY;
            tmpPieceFromCurrentPosition.currentPositionX = currentPositionX;
            if (tmpPieceFromTargetPosition != null) {
                tmpPieceFromTargetPosition.currentPositionY = targetPositionY;
                tmpPieceFromTargetPosition.currentPositionX = targetPositionX;
            }
            printBoard();
            return true;
        }
        Board[currentPositionY][currentPositionX] = null;
        Board[targetPositionY][targetPositionX] = null;
        Board[currentPositionY][currentPositionX] = tmpPieceFromCurrentPosition;
        Board[targetPositionY][targetPositionX] = tmpPieceFromTargetPosition;
        tmpPieceFromCurrentPosition.currentPositionX = currentPositionY;
        tmpPieceFromCurrentPosition.currentPositionY = currentPositionX;
        // if target position had a piece, return it
        if (tmpPieceFromTargetPosition != null) {
            tmpPieceFromTargetPosition.currentPositionY = targetPositionY;
            tmpPieceFromTargetPosition.currentPositionX = targetPositionX;
        }
        countValues();
        // System.out.println("Not in check");
        printBoard();
        return false;

    }

    /**
     * Used to set up board and images for starting position
     * @param gameGridPane GridPane for the game
     */
    public void setUpBoard(GridPane gameGridPane) {

        this.boardPane = gameGridPane;
        for (int i = 0; i < 8; i++) {
            Board[6][i] = new Pawn(Board, 6, i, 1);
            Pane paneFromNode = (Pane) returnNodefromNdx(6, i, gameGridPane);
            ImageView v = new ImageView(Board[6][i].returnImage(Board[6][i].getIsWhite()));
            v.setFitHeight(82);
            v.setFitWidth(109);
            paneFromNode.getChildren().add(v);

        }
        for (int yy = 0; yy < 8; yy++) {
            Board[1][yy] = new Pawn(Board, 1, yy, 0);
            Pane paneFromNode = (Pane) returnNodefromNdx(1, yy, gameGridPane);
            ImageView v = new ImageView(Board[1][yy].returnImage(Board[1][yy].getIsWhite()));
            v.setFitHeight(82);
            v.setFitWidth(109);
            paneFromNode.getChildren().add(v);
        }

        Board[0][4] = new King(Board, 0, 4, 0);
        blackKing = (King) Board[0][4];
        setPic(Board[0][4].returnImage(0), gameGridPane, 0, 4);
        Board[0][1] = new Knight(Board, 0, 1, 0);
        setPic(Board[0][1].returnImage(0), gameGridPane, 0, 1);
        Board[0][6] = new Knight(Board, 0, 6, 0);
        setPic(Board[0][6].returnImage(0), gameGridPane, 0, 6);
        Board[0][3] = new Queen(Board, 0, 3, 0);
        setPic(Board[0][3].returnImage(0), gameGridPane, 0, 3);
        Board[0][7] = new Rook(Board, 0, 7, 0);
        setPic(Board[0][7].returnImage(0), gameGridPane, 0, 7);
        Board[0][0] = new Rook(Board, 0, 0, 0);
        setPic(Board[0][0].returnImage(0), gameGridPane, 0, 0);
        Board[0][2] = new Bishop(Board, 0, 2, 0);
        setPic(Board[0][2].returnImage(0), gameGridPane, 0, 2);
        Board[0][5] = new Bishop(Board, 0, 5, 0);
        setPic(Board[0][5].returnImage(0), gameGridPane, 0, 5);
        Board[7][3] = new Queen(Board, 7, 3, 1);
        setPic(Board[7][3].returnImage(1), gameGridPane, 7, 3);
        Board[7][4] = new King(Board, 7, 4, 1);
        whiteKing = (King) Board[7][4];
        setPic(Board[7][4].returnImage(1), gameGridPane, 7, 4);
        Board[7][6] = new Knight(Board, 7, 6, 1);
        setPic(Board[7][6].returnImage(1), gameGridPane, 7, 6);
        Board[7][1] = new Knight(Board, 7, 1, 1);
        setPic(Board[7][1].returnImage(1), gameGridPane, 7, 1);
        Board[7][2] = new Bishop(Board, 7, 2, 1);
        setPic(Board[7][2].returnImage(1), gameGridPane, 7, 2);
        Board[7][5] = new Bishop(Board, 7, 5, 1);
        setPic(Board[7][5].returnImage(1), gameGridPane, 7, 5);
        Board[7][0] = new Rook(Board, 7, 0, 1);
        setPic(Board[7][0].returnImage(1), gameGridPane, 7, 0);
        Board[7][7] = new Rook(Board, 7, 7, 1);
        setPic(Board[7][7].returnImage(1), gameGridPane, 7, 7);

    }

    /**
     *
     * @param row Target row on gridpane
     * @param col Target col on gridpane
     * @param gameGridPane Gridpane used for graphics of the game
     * @return Node from row and col on gameGridPane
     */
    public Node returnNodefromNdx(int row, int col, GridPane gameGridPane) {
        Node r = null;
        if (gameGridPane != null) {
            ObservableList<Node> children = gameGridPane.getChildren();
            for (Node node : children) {
                if (gameGridPane.getRowIndex(node) == null) {

                } else if (gameGridPane.getRowIndex(node) == row && gameGridPane.getColumnIndex(node) == col) {
                    r = node;

                }
            }
        }
        return r;
    }

    /**
     * Sets an image at spesified node (by x, y position) in gridPane
     * @param img Image to set
     * @param gameGridPane Gridpane used for graphics of the game
     * @param row row to get node from gridpane
     * @param col column to get node from gridpane
     */
    void setPic(Image img, GridPane gameGridPane, int row, int col) {
        Pane panew = (Pane) returnNodefromNdx(row, col, gameGridPane);
        ImageView v = new ImageView(img);
        v.setFitHeight(82);
        v.setFitWidth(109);
        panew.getChildren().add(v);

    }

    /**
     *
     * @param gameGridPane
     * @param row row to remove image in node from gridpane
     * @param col column to remove image in node from gridpane
     */
    void removePic(GridPane gameGridPane, int row, int col) {
        Pane paneFromNode = (Pane) returnNodefromNdx(row, col, gameGridPane);
        paneFromNode.getChildren().clear();

    }

    /**
     * Setter for board array
     * @param board Piece[][] to set as game board
     */
    void setBoard(Piece[][] board) {
        this.Board = board;
    }

    /**
     * Counts the sum of white pieces - black pieces. Also updates hashSets containing all black and white pieces.
     * @return Total sum of white pieces - black pieces.
     */
    int countValues() {
        whitePieceSet.clear();
        blackPieceSet.clear();
        int tmp = 0;
        for (int i = 0; i < 8; i++) {
            for (int x = 0; x < 8; x++) {
                if (Board[i][x] != null) {
                    Piece p = Board[i][x];
                    if (p.getIsWhite() == 1) {
                        if (p instanceof King) {
                            whiteKing = (King) p;
                            whiteKing.currentPositionX = x;
                            whiteKing.currentPositionY = i;
                        }
                        whitePieceSet.add(Board[i][x]);
                    } else if (p.getIsWhite() == 0) {
                        if (p instanceof King) {
                            blackKing = (King) p;
                            blackKing.currentPositionX = x;
                            blackKing.currentPositionY = i;
                        }
                        blackPieceSet.add(Board[i][x]);
                    }
                    tmp = tmp + p.returnValue(p.getIsWhite());

                }
            }
        }
        return tmp;
    }

    /**
     * Returns dif between two ints
     * @param x First int to compare
     * @param y Second int to compare
     * @return Difference of two ints, or 0 if there is none.
     */
    public int compare(int x, int y) {
        int tmp = 0;
        if (x > y) {
            tmp = x - y;
        } else if (y > x) {
            tmp = y - x;
        }
        return tmp;
    }

    /**
     * Sets all pictures on gameGridPane
     * @param gameGridPane
     */
    void setAllPics(GridPane gameGridPane) {
        for (int i = 0; i < 8; i++) {
            for (int u = 0; u < 8; u++) {
                if (Board[i][u] != null) {
                    removePic(gameGridPane,i,u);
                    setPic(Board[i][u].returnImage(Board[i][u].getIsWhite()), gameGridPane, i, u);
                } else {
                    removePic(gameGridPane, i, u);
                }
            }
        }

    }

    /**
     * Generates legal moves for a side
     * @param Side Side to generate moves for
     * @return ArrayList of legal movePairs containing starting x, y and target x, y
     */
    ArrayList<movePair> generateAllLegalMoves(int Side) {
        ArrayList<movePair> values = new ArrayList();
        countValues();
        HashSet<Piece> ownSet = new HashSet<Piece>(returnOwnSet(Side));

        for (Piece p : ownSet) {
            for (int i = 0; i < 8; i++) {
                for (int n = 0; n < 8; n++) {
                    if (p.canMove(i, n, Board)) {
                        movePair pari = new movePair().generateMovePair(p.currentPositionX, p.currentPositionY, n, i);
                        //  System.out.println("generoidaan siirto " + returnNotation(pari));
                        values.add(pari);

                    }
                }

            }
        }
        // System.out.println(values.size() + "laillista siirtoa");

        return values;
    }

    /**
     * Returns all legal moves for a single Piece
     * @param pieceToGenerateMovesFor Piece for which legal moves are generated
     * @return ArrayList of legal movePairs containing starting x, y and target x, y
     */
    public ArrayList<movePair> returnLegalMoves(Piece pieceToGenerateMovesFor) {
        ArrayList<movePair> legalMovesToReturn = new ArrayList<>();
      if (pieceToGenerateMovesFor != null) {
          for (int i = 0; i < 8; i++) {
              for (int n = 0; n < 8; n++) {
                  if (pieceToGenerateMovesFor.canMove(i, n, Board)) {
                      movePair pair = new movePair().generateMovePair(pieceToGenerateMovesFor.currentPositionX, pieceToGenerateMovesFor.currentPositionY, n, i);
                      legalMovesToReturn.add(pair);

                  }
              }

          }

      }
        return legalMovesToReturn;
    }

    /**
     * Generates proper chess notation from movePair
     * @param pair movePair to convert into chess notation
     * @return Chess notation as a String from movePair
     */
    String returnNotation(movePair pair) {
        Integer targetyString = pair.targetY;
        String startingPosition = " ";
        String targetPosition = " ";

        switch (pair.startingY) {
            case 0 -> startingPosition = "8";
            case 1 -> startingPosition = "7";
            case 2 -> startingPosition = "6";
            case 3 -> startingPosition = "5";
            case 4 -> startingPosition = "4";
            case 5 -> startingPosition = "3";
            case 6 -> startingPosition = "2";
            case 7 -> startingPosition = "1";
        }
        startingPosition = switch (pair.startingX) {
            case 0 -> ("A " + startingPosition);
            case 1 -> ("B " + startingPosition);
            case 2 -> ("C " + startingPosition);
            case 3 -> ("D " + startingPosition);
            case 4 -> ("E " + startingPosition);
            case 5 -> ("F " + startingPosition);
            case 6 -> ("G " + startingPosition);
            case 7 -> ("H " + startingPosition);
            default -> startingPosition;
        };


        switch (pair.targetY) {
            case 0 -> targetPosition = "8";
            case 1 -> targetPosition = "7";
            case 2 -> targetPosition = "6";
            case 3 -> targetPosition = "5";
            case 4 -> targetPosition = "4";
            case 5 -> targetPosition = "3";
            case 6 -> targetPosition = "2";
            case 7 -> targetPosition = "1";
        }


        targetPosition = switch (pair.targetX) {
            case 0 -> (" A " + targetPosition);
            case 1 -> (" B " + targetPosition);
            case 2 -> (" C " + targetPosition);
            case 3 -> (" D " + targetPosition);
            case 4 -> (" E " + targetPosition);
            case 5 -> (" F " + targetPosition);
            case 6 -> (" G " + targetPosition);
            case 7 -> (" H " + targetPosition);
            default -> targetPosition;
        };

        return startingPosition + " to " + targetPosition;
    }

    /**
     * Returns hashSet of pieces for side provided
     * @param side 1 == white, 0 == black
     * @return HashSet containing pieces, 1 returns white, 0 returns black
     */
    HashSet<Piece> returnOwnSet(int side) {
        if (side == 1) {
            return whitePieceSet;
        } else {
            return blackPieceSet;
        }
    }


    /**
     * Deep copies the Piece objects to a new board
     * @param copyThis Array of Pieces to copy
     * @return Deep copy of Pieces
     */
    Piece[][] deepCopyPieces(Piece[][] copyThis) {
        Piece[][] returnThis = new Piece[8][8];
        for (Piece p : returnOwnSet(1)) {

            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Pawn) {
                Pawn pa = new Pawn(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Rook) {
                Rook pa = new Rook(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Bishop) {
                Bishop pa = new Bishop(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Knight) {
                Knight pa = new Knight(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof King) {
                King pa = new King(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Queen) {
                Queen pa = new Queen(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }


        }

        for (Piece p : returnOwnSet(0)) {

            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Pawn) {
                Pawn pa = new Pawn(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Rook) {
                Rook pa = new Rook(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Bishop) {
                Bishop pa = new Bishop(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Knight) {
                Knight pa = new Knight(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof King) {
                King pa = new King(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }
            if (copyThis[p.currentPositionY][p.currentPositionX] instanceof Queen) {
                Queen pa = new Queen(returnThis, p.currentPositionY, p.currentPositionX, p.getIsWhite());
                returnThis[pa.currentPositionY][pa.currentPositionX] = pa;
            }


        }
        return returnThis;
    }

    //  System.out.println("-----------------------------");
    // this.printBoard();
    //       System.out.println("----------------------");
    //    System.out.println(this.returnBoard()[0][0].getIsWhite());


}

class movePair {
    int startingX;
    int startingY;
    int targetX;
    int targetY;

    /**
     *
     * @param startingX Starting position on x axis
     * @param startingY Starting position on y axis
     * @param targetX Target position on x axis
     * @param targetY Target position on y axis
     * @return movePair containing start and end positions
     */
    movePair generateMovePair(int startingX, int startingY, int targetX, int targetY) {
        this.startingX = startingX;
        this.targetX = targetX;
        this.startingY = startingY;
        this.targetY = targetY;
        return this;
    }

}
