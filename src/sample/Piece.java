package sample;

import javafx.scene.image.Image;

abstract class Piece {
    Image piecePicture;
    Piece[][] board;
    boolean hasMoved = false;
    int whiteValue;
    int blackValue;
    int currentPositionX;
    int currentPositionY;

    private int isWhite; // 0 == black, 1 == white

    Piece(Piece board[][], int y, int x, int whiteOrBlack) {
        this.board = board;
        this.currentPositionX = x;
        this.currentPositionY = y;
        this.isWhite = whiteOrBlack;
    }

    /**
     * Checks if the move by piece is legal
     *
     * @param targetPositionY Target position on the y axis
     * @param targetPositionX Target position on the x axis
     * @param pieceArray      Array of pieces to check for legality
     * @return
     */
    abstract boolean canMove(int targetPositionY, int targetPositionX, Piece[][] pieceArray);

    /**
     * Moves piece from one place in the array representation of chessboard to another
     *
     * @param targetPositionY Target position on the y axis
     * @param targetPositionX Target position on the x axis
     * @param pieceArray      Array of pieces to check for legality
     */
    public void move(int currentPositionY, int currentPositionX, int targetPositionY, int targetPositionX, Piece[][] pieceArray) {

        // todo: Move castling logic entirely to King canMove. This might not work for recursion in minimax.

        if (this == null) {
            return;
        }

        if (this instanceof King) {
            int side = this.getIsWhite();
            if (((this.getIsWhite() == 1 && !this.hasMoved) && pieceArray[7][7] instanceof Rook) && ((targetPositionX == 6 && targetPositionY == 7) || (targetPositionX == 2 && targetPositionY == 7))) {
                if (targetPositionX == 6 && targetPositionY == 7) {
                    Rook tmpRook = (Rook) pieceArray[7][7];
                    if (!tmpRook.hasMoved) {
                        King tmpKing = (King) this;
                        tmpRook.move(7,7,7, 5, pieceArray);
                        pieceArray[currentPositionY][currentPositionX] = null;
                        pieceArray[targetPositionY][targetPositionX] = null;
                        pieceArray[targetPositionY][targetPositionX] = this;
                        this.currentPositionX = targetPositionX;
                        this.currentPositionY = targetPositionY;
                        tmpKing.hasMoved = true;
                        return;
                    }

                } else if ((targetPositionX == 2 && targetPositionY == 7) && pieceArray[7][0] instanceof Rook) {

                    if (pieceArray[7][0].canMove(this.currentPositionY, this.currentPositionX - 1, pieceArray)) {
                        Rook tmpRook = (Rook) pieceArray[7][0];
                        if (!tmpRook.hasMoved) {
                            King tmpKing = (King) this;
                            tmpRook.move(7,0,7, 3, pieceArray);
                            pieceArray[currentPositionY][currentPositionX] = null;
                            pieceArray[targetPositionY][targetPositionX] = null;
                            pieceArray[targetPositionY][targetPositionX] = this;
                            this.currentPositionX = targetPositionX;
                            this.currentPositionY = targetPositionY;
                            tmpKing.hasMoved = true;
                            return;
                        }
                    }
                }
            } else if ((this.getIsWhite() == 0 && !this.hasMoved) && ((targetPositionX == 2 && targetPositionY == 0)) || !this.hasMoved && (targetPositionX == 6 && targetPositionY == 0)) {


                if ((targetPositionX == 2 && targetPositionY == 0) && pieceArray[0][0] instanceof Rook) {
                    Rook tmpRook = (Rook) pieceArray[0][0];
                    if (!tmpRook.hasMoved) {
                        King tmpKing = (King) this;
                        tmpRook.move(0,0,0, 3, pieceArray);
                        pieceArray[currentPositionY][currentPositionX] = null;
                        pieceArray[targetPositionY][targetPositionX] = null;
                        pieceArray[targetPositionY][targetPositionX] = this;
                        this.currentPositionX = targetPositionX;
                        this.currentPositionY = targetPositionY;
                        tmpKing.hasMoved = true;
                        return;
                    }

                } else if ((targetPositionX == 6 && targetPositionY == 0) && pieceArray[0][7] instanceof Rook) {
                    Rook tmpRook = (Rook) pieceArray[0][7];
                    if (!tmpRook.hasMoved) {
                        King tmpKing = (King) this;
                        tmpRook.move(0,7,0, 5, pieceArray);
                        pieceArray[currentPositionY][currentPositionX] = null;
                        pieceArray[targetPositionY][targetPositionX] = null;
                        pieceArray[targetPositionY][targetPositionX] = this;
                        this.currentPositionX = targetPositionX;
                        this.currentPositionY = targetPositionY;
                        tmpKing.hasMoved = true;
                        return;
                    }
                }
            } else {
                this.hasMoved = true;
                pieceArray[currentPositionY][currentPositionX] = null;
                pieceArray[targetPositionY][targetPositionX] = null;
                pieceArray[targetPositionY][targetPositionX] = this;
                this.currentPositionX = targetPositionX;
                this.currentPositionY = targetPositionY;
                return;
            }


        }
        pieceArray[targetPositionY][targetPositionX] = this;
        pieceArray[currentPositionY][currentPositionX] = null;
        this.hasMoved = true;
        this.currentPositionX = targetPositionX;
        this.currentPositionY = targetPositionY;

    }

    /**
     * @return Name of the piece
     */
    abstract String returnName();

    /**
     * @param whiteOrBlack 1 == white, 0 == black.
     * @return Value of the piece white == positive, black == negative
     */
    abstract int returnValue(int whiteOrBlack);

    /**
     * @param whiteOrBlack 1 == white, 0 == black.
     * @return Image of the piece
     */
    abstract Image returnImage(int whiteOrBlack);

    /**
     * @return 0 if black, 1 if white
     */
    int getIsWhite() {
        return this.isWhite;
    }

    /**
     * Checks if the starting piece belongs to the same side as target piece
     *
     * @param pieceCurrentPositionX x coordinate of piece to be moved
     * @param pieceCurrentPositionY y coordinate of piece to be moved
     * @param pieceTargetPositionX  x coordinate of target
     * @param pieceTargetPositionY  y coordinate of target
     * @param board                 8x8 Array of pieces
     * @return returns true if targets value is the same as staring positions
     */
    boolean isOwn(int pieceCurrentPositionX, int pieceCurrentPositionY, int pieceTargetPositionX, int pieceTargetPositionY, Piece[][] board) {
        if (board[pieceCurrentPositionX][pieceCurrentPositionY] != null)
            if (board[pieceTargetPositionX][pieceTargetPositionY] != null) {
                if (board[pieceCurrentPositionX][pieceCurrentPositionY].isWhite == board[pieceTargetPositionX][pieceTargetPositionY].isWhite) {
                    //     System.out.println("You cant capture own pieces");
                    return true;
                }
            }
        return false;
    }


    /**
     * Returns the difference between two ints
     *
     * @param x f
     * @param y
     * @return
     */
    public int compare(int x, int y) {
        int tmp = 0;
        if (x > y) {
            tmp = x - y;
        } else if (y > x) {
            tmp = y - x;
        }
        return tmp;
    }

    /**
     * Checks if diagonal move is valid
     * Used for Pieces Queen and bishop
     *
     * @param targetY target position on y diagonal
     * @param targetX target position on x diagonal
     * @param b
     * @return returns true if diagonal move is valid, false if invalid
     */
    public boolean diagonalMoveIsValid(int targetY, int targetX, Piece[][] b) {

        if (this.currentPositionY == targetY && this.currentPositionX == targetX) {
            //   System.out.println("Bishop can only move diagonally");
            return false;
        }
        if (Math.abs(this.currentPositionX - targetX) != Math.abs(this.currentPositionY - targetY)) {
            return false;
        }
        int colOffSet, rowOffSet;
        if (this.currentPositionX < targetX) {
            colOffSet = 1;
        } else {
            colOffSet = -1;
        }

        if (this.currentPositionY < targetY) {
            rowOffSet = 1;
        } else {
            rowOffSet = -1;
        }

        if (b[targetY][targetX] != null && b[this.currentPositionY][this.currentPositionX] != null) {
            if (b[targetY][targetX].isOwn(this.currentPositionY, this.currentPositionX, targetY, targetX, b)) {
                return false;
            }
        }

        if (Math.abs((this.currentPositionX - targetX)) == Math.abs(this.currentPositionY - targetY)) {
            int xo = colOffSet + this.currentPositionX;
            for (int yo = rowOffSet + this.currentPositionY; yo != targetY; yo += rowOffSet) {
                if (b[yo][xo] != null) {
                    return false;
                }
                xo += colOffSet;
            }

            return true;

        }


        return false;


    }

    /**
     * Checks if horizontal move is valid
     * Used for Pieces Queen and Rook
     *
     * @param targetPositionY target position on y diagonal
     * @param targetPositionX target position on x diagonal
     * @param b
     * @return True if horizontal move is valid, else false
     */
    public boolean horizontalMoveIsValid(int targetPositionY, int targetPositionX, Piece[][] b) {

        if (b[targetPositionY][targetPositionX] != null) {
            if (b[targetPositionY][targetPositionX].getIsWhite() == this.getIsWhite()) {
                return false;
            }
        }
        int offset;
        if (this.currentPositionX != targetPositionX && this.currentPositionY != targetPositionY) {
            return false;
        }
        if (this.currentPositionY != targetPositionY) {
            if (this.currentPositionY < targetPositionY) {
                offset = 1;
            } else {
                offset = -1;
            }
            for (int i = this.currentPositionY + offset; i != targetPositionY; i += offset) {
                if (b[i][this.currentPositionX] != null) {
                    return false;
                }

            }
            return true;

        } else if (this.currentPositionX != targetPositionX) {
            if (this.currentPositionX < targetPositionX) {
                offset = 1;
            } else {
                offset = -1;
            }
            for (int i = this.currentPositionX + offset; i != targetPositionX; i += offset) {
                if (b[this.currentPositionY][i] != null) {
                    return false;
                }

            }
            return true;

        }
        return false;

    }


}

class Pawn extends Piece {
    int blackValue = -1;
    int whiteValue = 1;

    Image blackPawnPicture = new Image(getClass().getResource("/images/blackPawn.png").toString());
    Image whitePawnPicture = new Image(getClass().getResource("/images/whitePawn.png").toString());
    String name = "P";
    Piece[][] b;
    int whiteOrBlack;

    public Pawn(Piece[][] board, int currentPositionY, int currentPositionX, int whiteOrBlack) {
        super(board, currentPositionY, currentPositionX, whiteOrBlack);
    }

    @Override
    boolean canMove(int targetPositionY, int targetPositionX, Piece[][] b) {
        int currentY = this.currentPositionY;
        int currentX = this.currentPositionX;

        if (isOwn(this.currentPositionY, this.currentPositionX, targetPositionY, targetPositionX, b)) {
            // System.out.println("Cant capture own piece");
            return false;
        }
        if (currentX != targetPositionX && (b[targetPositionY][targetPositionX] == null)) {
            // System.out.println("Pawn can only move forwards white debug");
            return false;
        }
        if ((this.getIsWhite() == 0 && targetPositionY < currentY) || (this.getIsWhite() == 1 && targetPositionY > currentY)) {
            // System.out.println("Pawn can only move forwards black debug");
            return false;
        }
        if (compare(targetPositionY, currentY) > 2) {
            return false;
        }
        if (compare(targetPositionX, currentX) > 2) {
            return false;
        }
        if ((this.hasMoved && compare(targetPositionY, currentY) > 1) || (this.hasMoved && compare(targetPositionY, currentY) > 1)) {
            // System.out.println("Hasmoved and to far");
            return false;
        }

        int dif = compare(currentX, targetPositionX);
        int ydif = compare(currentY, targetPositionY);

        if ((b[targetPositionY][targetPositionX] == null && (b[currentY][currentX] != null)) && (dif < 2 || ((this.hasMoved == false && dif <= 2)))) {
            if (dif == 2) {
                if (this.getIsWhite() == 1) {
                    if (b[currentX][currentY - 1] != null) {
                        // System.out.println("something in the way");
                        return false;

                    }
                }
                if (this.getIsWhite() == 0) {
                    if (b[currentX][currentY + 1] != null) {
                        //  System.out.println("something in the way");
                        return false;

                    }
                }

            }
            if (b[currentY][currentX].getIsWhite() == 0 && targetPositionY < currentY) {
                return false;
            } else if (b[currentY][currentX].getIsWhite() == 1 && targetPositionY > currentY) {
                return false;
            }

            // System.out.println("tst" + currentY + " " + currentX);
            return true;
        } else if (b[targetPositionY][targetPositionX] != null && b[this.currentPositionY][this.currentPositionX] != null) {

            if ((b[targetPositionY][targetPositionX].getIsWhite() != b[this.currentPositionY][this.currentPositionX].getIsWhite()) && ((ydif == 1 && dif == 1))) {
                return true;

            }
        }

        //   System.out.println("Illegal move");
        return false;
    }


    @Override
    String returnName() {
        return this.name;
    }

    int returnValue(int whiteOrBlack) {
        if (whiteOrBlack == 1) {
            return this.whiteValue;
        }
        return this.blackValue;
    }


    /**
     * @param whiteOrBlack side of piece in question, white == 1, black == 0
     * @return returns Image of pawn depending on value, white == 1, black == 0
     */
    @Override
    Image returnImage(int whiteOrBlack) {
        if (whiteOrBlack == 0) {
            return blackPawnPicture;
        }
        return whitePawnPicture;
    }

}

class Rook extends Piece {

    int blackValue = -5;
    int whiteValue = 5;
    Image blackrookPicture = new Image(getClass().getResource("/images/blackRook.png").toString());
    Image whiteRookPicture = new Image(getClass().getResource("/images/whiteRook.png").toString());
    String name = "R";
    Piece[][] b;
    int whiteOrBlack;

    public Rook(Piece[][] board, int y, int x, int whiteOrBlack) {
        super(board, y, x, whiteOrBlack);
    }

    @Override
    boolean canMove(int targetY, int targetX, Piece[][] b) {
        return this.horizontalMoveIsValid(targetY, targetX, b);
    }

    @Override
    String returnName() {
        return this.name;
    }

    @Override
    int returnValue(int whiteOrBlack) {
        if (whiteOrBlack == 1) {
            return this.whiteValue;
        }
        return this.blackValue;
    }

    @Override
    Image returnImage(int whiteOrBlack) {
        if (whiteOrBlack == 0) {
            return blackrookPicture;
        }
        return whiteRookPicture;
    }


}

class Bishop extends Piece {
    int blackValue = -3;
    int whiteValue = 3;
    Image blackBishopPicture = new Image(getClass().getResource("/images/blackBishop.png").toString());
    Image whiteBishopPicture = new Image(getClass().getResource("/images/whiteBishop.png").toString());
    String name = "B";

    public Bishop(Piece[][] board, int y, int x, int whiteOrBlack) {
        super(board, y, x, whiteOrBlack);
    }

    @Override
    boolean canMove(int targetY, int targetX, Piece[][] b) {

        return this.diagonalMoveIsValid(targetY, targetX, b);
    }

    @Override
    String returnName() {
        return this.name;
    }

    @Override
    int returnValue(int whiteOrBlack) {
        if (whiteOrBlack == 1) {
            return this.whiteValue;
        }
        return this.blackValue;
    }

    @Override
    Image returnImage(int whiteOrBlack) {
        if (whiteOrBlack == 0) {
            return blackBishopPicture;
        }
        return whiteBishopPicture;
    }


}

class Queen extends Piece {
    int blackValue = -9;
    int whiteValue = 9;
    Image blackQueenPicture = new Image(getClass().getResource("/images/blackQueen.png").toString());
    Image whiteQueenPicture = new Image(getClass().getResource("/images/whiteQueen.png").toString());
    String name = "Q";

    public Queen(Piece[][] board, int y, int x, int whiteOrBlack) {
        super(board, y, x, whiteOrBlack);
    }

    @Override
    boolean canMove(int targetPositionY, int targetPositionX, Piece[][] b) {

        if (this.currentPositionX != targetPositionX && this.currentPositionY != targetPositionY) {
            if (Math.abs((this.currentPositionX - targetPositionX)) == Math.abs(this.currentPositionY - targetPositionY)) {
                return this.diagonalMoveIsValid(targetPositionY, targetPositionX, b);
            }
        }
        boolean inLine = true;
        if (this.currentPositionX != targetPositionX && this.currentPositionY != targetPositionY) {
            inLine = false;
        }
        if (inLine) {
            return this.horizontalMoveIsValid(targetPositionY, targetPositionX, b);
        }
        return false;

    }

    @Override
    String returnName() {
        return this.name;
    }

    @Override
    int returnValue(int whiteOrBlack) {
        if (whiteOrBlack == 1) {
            return this.whiteValue;
        }
        return this.blackValue;
    }

    @Override
    Image returnImage(int whiteOrBlack) {
        if (whiteOrBlack == 0) {
            return blackQueenPicture;
        }
        return whiteQueenPicture;
    }

}

class King extends Piece {
    int blackValue = -999999;
    int whiteValue = 999999;
    Image blackKingPicture = new Image(getClass().getResource("/images/blackKing.png").toString());
    Image whiteKingPicture = new Image(getClass().getResource("/images/whiteKing.png").toString());
    String name = "K";

    public King(Piece[][] board, int y, int x, int whiteOrBlack) {
        super(board, y, x, whiteOrBlack);
    }


    @Override
    boolean canMove(int targetPositionY, int targetPositionX, Piece[][] b) {
        int currentY = this.currentPositionY;
        int currentX = this.currentPositionX;

        // Checks for castling rights
        if (!this.hasMoved) {
            if (((targetPositionX == 6 && targetPositionY == 7) || (targetPositionX == 2 && targetPositionY == 7)) || ((targetPositionX == 6 && targetPositionY == 0) || (targetPositionX == 2 && targetPositionY == 0))) {
                if (this.getIsWhite() == 1) {
                    if (targetPositionX == 6 && targetPositionY == 7) {
                        if (b[7][7] != null) {
                            if (b[7][7] instanceof Rook && !b[7][7].hasMoved) {
                                if (b[7][7].canMove(this.currentPositionY, this.currentPositionX + 1, b)) {
                                    return true;
                                }
                            }
                        }

                    } else if (targetPositionX == 2 && targetPositionY == 7) {
                        if (b[7][0] != null) {
                            if (!b[7][0].hasMoved && b[7][0] instanceof Rook) {
                                if (b[7][0].canMove(this.currentPositionY, this.currentPositionX - 1, b)) {
                                    return true;
                                }
                            }
                        }


                    }
                } else if (this.getIsWhite() == 0) {

                    if (targetPositionX == 6 && targetPositionY == 0) {
                        if (b[0][7] != null) {
                            if (b[0][7] instanceof Rook && !b[0][7].hasMoved) {
                                if (b[0][7].canMove(this.currentPositionY, this.currentPositionX + 1, b)) {
                                    return true;
                                }
                            }
                        }

                    } else if (targetPositionX == 2 && targetPositionY == 0) {
                        if (b[0][0] != null) {
                            if (!b[0][0].hasMoved && b[0][0] instanceof Rook) {
                                if (b[0][0].canMove(this.currentPositionY, this.currentPositionX - 1, b)) {
                                    return true;
                                }
                            }
                        }


                    }
                }


            }
        }
        if (b[currentY][currentX] != null) {
            if (b[targetPositionY][targetPositionX] == null || (!b[currentY][currentX].isOwn(currentY, currentX, targetPositionY, targetPositionX, b))) {

                if (((targetPositionY == currentY + 1 || targetPositionY == currentY - 1) && targetPositionX == currentX)) {
                    return true;

                } else if (((targetPositionX == currentX + 1 || targetPositionX == currentX - 1) && targetPositionY == currentY)) {
                    return true;

                } else if (Math.abs((this.currentPositionY - targetPositionY)) == Math.abs(this.currentPositionX - targetPositionX)) {
                    if (this.compare(currentY, targetPositionY) > 1 || this.compare(currentX, targetPositionX) > 1) {

                        return false;
                    }
                    return true;

                }
            }

        }
        return false;
    }


    @Override
    String returnName() {
        return this.name;
    }

    @Override
    int returnValue(int whiteOrBlack) {
        if (whiteOrBlack == 1) {
            return this.whiteValue;
        }
        return this.blackValue;
    }

    @Override
    Image returnImage(int whiteOrBlack) {
        if (whiteOrBlack == 0) {
            return blackKingPicture;
        }
        return whiteKingPicture;
    }

}

class Knight extends Piece {
    int blackValue = -3;
    int whiteValue = 3;
    Image blacKnightPicture = new Image(getClass().getResource("/images/blackKnight.png").toString());
    Image whiteKnightpicture = new Image(getClass().getResource("/images/whiteKnight.png").toString());
    String name = "N";

    public Knight(Piece[][] board, int currentPositionY, int currentPositionX, int whiteOrBlack) {
        super(board, currentPositionY, currentPositionX, whiteOrBlack);
    }

    @Override
    boolean canMove(int targetPositionY, int targetPositionX, Piece[][] b) {

        if (b[targetPositionY][targetPositionX] != null) {
            if (this.isOwn(this.currentPositionY, this.currentPositionX, targetPositionY, targetPositionX, b)) {
                return false;
            }
        }
        if ((this.currentPositionY + 2 == targetPositionY || targetPositionY == this.currentPositionY - 2) && (targetPositionX == this.currentPositionX + 1 || targetPositionX == this.currentPositionX - 1)) {

            return true;

        } else if ((this.currentPositionX + 2 == targetPositionX || targetPositionX == this.currentPositionX - 2) && (targetPositionY == this.currentPositionY + 1 || targetPositionY == this.currentPositionY - 1)) {

            return true;

        }

        return false;

    }

    @Override
    String returnName() {
        return this.name;
    }

    @Override
    int returnValue(int whiteOrBlack) {
        if (whiteOrBlack == 1) {
            return this.whiteValue;
        }
        return this.blackValue;
    }

    @Override
    Image returnImage(int whiteOrBlack) {
        if (whiteOrBlack == 0) {
            return blacKnightPicture;
        }
        return whiteKnightpicture;
    }

}

