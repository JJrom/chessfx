package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class chessClock {
    @FXML
    private TextField whiteTimeLeftTextField;
    @FXML
    private TextField blackTimeLeftTextField;
    boolean isWhite = true;
    private long blackTimeLeft = 900000;
    private long whiteTimeLeft = 900000;
    private long currentTime = System.currentTimeMillis();
    Timer chessClock = new Timer();
    Timer blackChessClock = new Timer();

    TimerTask tt = new TimerTask() {
        @Override
        public void run() {
            updateTime();
        }

        ;
    };

    public void startChessClock(TextField blackField, TextField whiteField) {
        this.blackTimeLeftTextField = blackField;
        this.whiteTimeLeftTextField = whiteField;
        chessClock.scheduleAtFixedRate(tt, 1000, 1000);
    }

    public long returnTime(boolean isWhite) {
        if (isWhite) {
            return this.whiteTimeLeft;

        } else {
            return this.blackTimeLeft;
        }
    }

    public void changeIsWhite() {
        if (isWhite) {
            isWhite = false;
        }else {
            isWhite = true;
        }
    }

    public void updateTime() {
        if (isWhite) {
            long ellapsedTime = System.currentTimeMillis() - currentTime;
            whiteTimeLeft = whiteTimeLeft - ellapsedTime;
            whiteTimeLeftTextField.setText(Long.toString(TimeUnit.MILLISECONDS.toSeconds(returnTime(isWhite))));
        } else {
            long ellapsedTime = System.currentTimeMillis() - currentTime;
            blackTimeLeft = blackTimeLeft - ellapsedTime;
            blackTimeLeftTextField.setText(Long.toString(TimeUnit.MILLISECONDS.toSeconds(returnTime(isWhite))));
        }
        currentTime = System.currentTimeMillis();

    }
}

