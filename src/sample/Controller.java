package sample;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

public class Controller {
    Pane privatePane = null;
    String privateBackGroundStyle = null;
    ArrayList<nodeStylePair> nodeStylePairList = new ArrayList<>();
    int row;
    int col;
    int playedTurns = 0;
    boolean selected = false;
    Game gameLogic = new Game();
    gameInfo infoForGame = new gameInfo();
    chessClock ClockForChess = new chessClock();

    @FXML
    private TextField whiteTimeLeftTextField;
    @FXML
    private TextField blackTimeLeftTextField;
    @FXML
    private AnchorPane mainPane;
    @FXML
    private GridPane boardGridPane;
    @FXML
    private LineChart chartEval;
    @FXML
    private TextField turnTextField;
    @FXML
    private TextArea displayMovesTextField;
    miniMaxEngine engine;

    @FXML
    private void initialize() {
        gameLogic.setUpBoard(boardGridPane);
        ClockForChess.startChessClock(blackTimeLeftTextField, whiteTimeLeftTextField);
        // engine = new miniMaxEngine(b, boardGridPane);
    }

    @FXML
    private void SelectOnGridPanelAction(MouseEvent event) {
        for (nodeStylePair pa : nodeStylePairList) {
            pa.setNodeStyle();
        }
        nodeStylePairList.clear();
        System.out.println("Selected" + selected);
        gameLogic.printBoard();
        Node selectedNode = (Node) event.getSource();
        int col = GridPane.getColumnIndex(selectedNode);
        int row = GridPane.getRowIndex(selectedNode);
        if (!selected) {
            if (privatePane != null) {
                privatePane.setStyle(privateBackGroundStyle);
                privatePane = null;
                privateBackGroundStyle = null;
            }

            selected = true;
            this.row = row;
            this.col = col;
            Pane paneFromNode = (Pane) selectedNode;
            privateBackGroundStyle = paneFromNode.getStyle();

            selectedNode.setStyle("-fx-background-color: #DECCBA");
            System.out.println(row);
            System.out.println(col);
            ArrayList<movePair> Pairs = gameLogic.returnLegalMoves(gameLogic.returnBoard()[row][col]);

            for (movePair pair : Pairs) {
                for (Node n : boardGridPane.getChildren()) {
                    if (boardGridPane.getRowIndex(n) == pair.targetY && boardGridPane.getColumnIndex(n) == pair.targetX) {
                        Pane x = (Pane) n;
                        nodeStylePair pairToAdd = new nodeStylePair(n, x.getStyle());
                        nodeStylePairList.add(pairToAdd);

                        System.out.println(n == null);
                        n.setStyle("-fx-background-color: #DECCBA");

                    }

                }
            }

            privatePane = paneFromNode;
            this.row = row;
            this.col = col;
            selected = true;
            gameLogic.printBoard();

        } else if (gameLogic.hasPiece(this.row, this.col) && selected) {

            if ((infoForGame.whiteTurn == true && gameLogic.returnBoard()[this.row][this.col].getIsWhite() == 1) || (infoForGame.whiteTurn == false && gameLogic.returnBoard()[this.row][this.col].getIsWhite() == 0)) {
                gameLogic.countValues();
                gameLogic.printBoard();
                System.out.println();
                if (gameLogic.returnBoard()[this.row][this.col].canMove(row, col, gameLogic.returnBoard())) {
                    if (gameLogic.movePiece(this.row, this.col, row, col, boardGridPane)) {
                        movePair m = new movePair();
                        m.startingY = this.row;
                        m.startingX = this.col;
                        m.targetY = row;
                        m.targetX = col;

                        System.out.println(gameLogic.returnNotation(m));
                        infoForGame.changeTurn();
                        ClockForChess.changeIsWhite();
                        XYChart.Series series = new XYChart.Series();
                        series.setName("Turn");
                        series.getData().add(new XYChart.Data(playedTurns, gameLogic.countValues()));
                        chartEval.getData().add(series);
                        turnTextField.setText(infoForGame.returnTurnString());
                        playedTurns++;

                    }
                }
                if (privatePane != null) {
                    privatePane.setStyle(privateBackGroundStyle);
                    privatePane = null;
                    privateBackGroundStyle = null;
                }
            }
            selected = false;
            displayMovesTextField.clear();

        } else {

            if (privatePane != null) {
                privatePane.setStyle(privateBackGroundStyle);
                privatePane = null;
                privateBackGroundStyle = null;
            }

            displayMovesTextField.clear();
            selected = false;

        }
        gameLogic.countValues();
        gameLogic.setAllPics(boardGridPane);
    }

    class nodeStylePair {
        Node n;
        String b;

        public nodeStylePair(Node node, String styleOfNode) {
            this.n = node;
            this.b = styleOfNode;
        }

        /**
         * Sets the previously saved style to square. Used to revert highlighting legal moves
         */
        void setNodeStyle() {
            Pane x = (Pane) n;

            x.setStyle(b);
        }

    }


}
