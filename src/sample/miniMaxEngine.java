package sample;

import javafx.scene.layout.GridPane;

import java.util.ArrayList;

public class miniMaxEngine {
    // Very much wip.
    Game game;
    GridPane gameGridPane;
    int side = 0;
    boolean checkMated = false;

    miniMaxEngine(Game game, GridPane gameGridPane) {
        this.game = game;
        this.gameGridPane = gameGridPane;
    }

    void makeAMove(int side) {
        boolean moveMade = false;
        ArrayList<movePair> moves = new ArrayList<>(game.generateAllLegalMoves(side));
        // for (movePair pari : moves) {
        //System.out.println(pari.startingX + " " + pari.startingY +" " + pari.targetX + " " + pari.targetY);
        // }
        //System.out.println(moves);

        while (!moveMade) {

            if (moves.size() == 0) {
                this.checkMated = true;
                return;

            }
            // System.out.println(moves.size()+ "siirtoja");
            movePair p = getBestmove(moves, side);
            if (p == null) {
                System.out.println("Checkmate");
                checkMated = true;
                return;

            }
            // System.out.println("Paras siirto oli" + g.returnNotation(p));

            // Needs to be done with canMove when actually implementing
            moveMade = game.movePiece(p.startingY, p.startingX, p.targetY, p.targetX, gameGridPane);

            System.out.println(p.targetY + "" + p.startingX);
            if (!moveMade) {
                System.out.println("removing" + game.returnNotation(p));
                moves.remove(p);
            } else {
                game.setAllPics(gameGridPane);
                game.printBoard();
                break;
            }

        }
    }

    movePair getBestmove(ArrayList<movePair> l, int side) {
        movePair returnthis = null;

        int value = 10000000;
        if (side == 1) {
            value = -10000000;
        }
        int cvalue;
        int count = l.size();
        for (movePair p: l) {
          //  System.out.println("jäljellä" + count);
            count--;

            Piece tmpPieceFrom = game.Board[p.startingY][p.startingX];


            Piece tmpPieceTo = game.Board[p.targetY][p.targetX];
            boolean tmpToHasMoved = false;
            boolean tmpFromHasMoved = tmpPieceFrom.hasMoved;
            boolean legalMove = false;
            if (tmpPieceTo != null) {
                 tmpToHasMoved = tmpPieceTo.hasMoved;
            } else {
                 tmpToHasMoved = false;
            }

            // Won't be needed when in check logic migrated to King
            legalMove = game.movePiece(p.startingY, p.startingX, p.targetY, p.targetX, gameGridPane);

            cvalue = game.countValues();
            if ((cvalue<value && legalMove) && side == 0 ||(cvalue>value && legalMove) && side == 1)  {
                value = cvalue;
                returnthis = p;
                // System.out.println("uusi paras siirto " + g.returnNotation(p) +" arvo "+ value);
            }

            if (tmpPieceTo != null) {
                tmpPieceTo.hasMoved = tmpToHasMoved;
                tmpPieceTo.currentPositionX = p.targetX;
                tmpPieceTo.currentPositionY = p.targetY;
            }
            game.Board[p.startingY][p.startingX] = tmpPieceFrom;
            tmpPieceFrom.hasMoved = tmpFromHasMoved;
            tmpPieceFrom.currentPositionY = p.startingY;
            tmpPieceFrom.currentPositionX = p.startingX;
            game.Board[p.targetY][p.targetX] = tmpPieceTo;

            game.countValues();

        }
        return returnthis;
    }


}
