package sample;

import java.util.Timer;

public class gameInfo {
    boolean whiteTurn = true;
    String returnTurnString(){
        if (this.whiteTurn) {
            return "White to move";
        }
        return "Black to move";
    }
    public void changeTurn() {
        if (this.whiteTurn == true) {
            this.whiteTurn = false;
        } else {
            this.whiteTurn = true;
        }
    }

    public boolean returnTurn() {
        return this.whiteTurn;
    }
}
